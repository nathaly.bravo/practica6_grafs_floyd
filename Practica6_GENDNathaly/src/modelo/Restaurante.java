/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;

/**
 *
 * @author Nathaly
 */
public class Restaurante implements Serializable{
    private Integer id;
    private String nombre;
    private String Ubicacion;

    public Restaurante() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacion() {
        return Ubicacion;
    }

    public void setUbicacion(String Ciudad) {
        this.Ubicacion = Ciudad;
    }

    public Restaurante(Integer id, String nombre, String Ubicacion) {
        this.id = id;
        this.nombre = nombre;
        this.Ubicacion = Ubicacion;
    }
    
    @Override
    public String toString() {
        return  nombre+ " - " + Ubicacion;
    }
    
    
}
