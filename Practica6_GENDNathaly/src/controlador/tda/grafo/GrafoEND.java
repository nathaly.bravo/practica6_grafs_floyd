/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.tda.grafo;

import controlador.tda.grafo.exception.VerticeException;
import java.io.Serializable;
import static java.nio.file.Files.size;

/**
 *
 * @author Nathaly
 * @param <E>
 */
public class GrafoEND <E> extends GrafoDE<E> implements Serializable{
        
    /**
     *
     * @param numVertice
     * @param clazz
     */
    public GrafoEND(Integer numVertice, Class clazz){
        super(numVertice, clazz);
    }

    @Override
    public void insertarArista(Integer i, Integer j, Double peso) throws VerticeException {
        if (i > 0 && j > 0 && i <= numV && j <= numV) {
            Object[] existe = existeArista(i, j);
            if (!((Boolean) existe[0])) {
                numA++;
                listaAdyacente[i].insertarCabecera(new Adycencia(j, peso));
                listaAdyacente[j].insertarCabecera(new Adycencia(i, peso));
            }
        } else {
            throw new VerticeException("Algun vertice ingresado no existe GND");

        }
    }

    @Override
    public void insertarAristaE(E i, E j, Double peso) throws Exception {
        insertarArista(obtenerCodigo(i), obtenerCodigo(j), peso);
        insertarArista(obtenerCodigo(j), obtenerCodigo(i), peso);
    }
   
}
