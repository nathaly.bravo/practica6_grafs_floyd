/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.tda.grafo;

import controlador.tda.cola.Cola;
import controlador.tda.cola.ColaServices;
import controlador.tda.grafo.exception.GrafoConexionException;
import controlador.tda.grafo.exception.VerticeException;
import controlador.tda.lista.ListaEnlazada;
import controlador.tda.lista.exception.EstructuraDataVaciaExpetion;
import controlador.tda.lista.exception.PosicionException;
import controlador.tda.lista.exception.TopeException;

/**
 *
 * @author jona
 */
public abstract class Grafo {

    public abstract Integer numVertices();

    public abstract Integer numAristas();

    public abstract Object[] existeArista(Integer i, Integer f) throws VerticeException;

    public abstract Double pesoArista(Integer i, Integer f) throws VerticeException;

    public abstract void insertarArista(Integer i, Integer j) throws VerticeException;

    public abstract void insertarArista(Integer i, Integer j, Double peso) throws VerticeException;

    public abstract ListaEnlazada<Adycencia> adycentes(Integer i) throws VerticeException;

    protected float[][] Matriz;
    protected int[] restaurantesi;
    protected int ordenres;
    protected Cola<Integer> cola;

    @Override
    public String toString() {
        StringBuilder grafo = new StringBuilder();
        for (int i = 1; i <= numVertices(); i++) {
            grafo.append("VERTICE " + i);
            try {
                ListaEnlazada<Adycencia> lista = adycentes(i);
                for (int j = 0; j < lista.getSize(); j++) {
                    Adycencia aux = lista.obtenerDato(j);
                    if (aux.getPeso().toString().equalsIgnoreCase(String.valueOf(Double.NaN))) {
                        grafo.append("----VERTICE DESTINO " + aux.getDestino());
                    } else {
                        grafo.append("----VERTICE DESTINO " + aux.getDestino() + " peso " + aux.getPeso());
                    }

                }
                grafo.append("\n");

            } catch (Exception e) {
                System.out.println("Error en toString");
            }
        }
        return grafo.toString();
    }

    private Boolean estaConectado() {
        Boolean bad = true;
        for (int i = 1; i >= numVertices(); i++) {
            try {
                ListaEnlazada<Adycencia> lista = adycentes(i);
                if (lista.getSize() == 0) {
                    bad = false;
                    break;
                }
            } catch (Exception e) {
                System.out.println("Error en toString" + e);
                bad = false;
            }
        }
        return bad;
    }

    private Boolean estaPintado(ListaEnlazada<Integer> lista, Integer vertice) throws PosicionException {
        Boolean band = false;
        for (int i = 0; i < lista.getSize(); i++) {
            if (lista.obtenerDato(i).intValue() == vertice.intValue()) {
                band = true;
                break;
            }
        }
        return band;

    }

    public ListaEnlazada caminoMinimo(Integer verticeInicial, Integer VerticeFinal) throws GrafoConexionException, PosicionException {
        ListaEnlazada<Integer> camino = new ListaEnlazada();
        if (estaConectado()) {
            ListaEnlazada pesos = new ListaEnlazada();
            Boolean finalizar = false;
            Integer inicial = verticeInicial;
            camino.insertarCabecera(inicial);
            while (!finalizar) {
                ListaEnlazada<Adycencia> adyacencias = new ListaEnlazada();
                Integer T = -1;
                Double peso = 100000000.0;
                for (int i = 0; i < adyacencias.getSize(); i++) {
                    Adycencia ad = adyacencias.obtenerDato(i);
                    if (estaPintado(camino, ad.getDestino())) {
                        Double pesoArista = ad.getPeso();
                        if (VerticeFinal.intValue() == ad.getDestino()) {
                            T = ad.getDestino();
                            peso = ad.getPeso();
                            break;
                        } else if (pesoArista < peso) {
                            T = ad.getDestino();
                            peso = pesoArista;
                        }
                    }
                }
                if (T > -1) {
                    pesos.insertarCabecera(peso);
                    camino.insertarCabecera(T);
                    inicial = T;
                } else {
                    throw new GrafoConexionException("NO SE ENCUENTRA EL CAMINO");
                }
                if (verticeInicial.intValue() == inicial.intValue()) {
                    finalizar = true;
                }
            }
        } else {
            throw new GrafoConexionException("EL GRAFO NO ESTA CONECTADO");
        }
        return camino;
    }

    //Metodo generar matriz
    public float[][] GenerarM() throws Exception {
        Matriz = new float[numVertices()][numVertices()];

        for (int i = 0; i < numVertices(); i++) {
            ListaEnlazada<Adycencia> ad = adycentes((i + 1));
            for (int j = 0; j < ad.getSize(); j++) {
                Adycencia aux = ad.obtenerDato(j);
                if (aux.getPeso() != null) {
                    Matriz[i][aux.getDestino() - 1] = ad.obtenerDato(j).getPeso().floatValue();
                }
                if (ad.obtenerDato(j).getPeso() == null) {
                    Matriz[i][j] = -1;
                }
                if (i == j) {
                    Matriz[i][j] = 0;
                }
            }
        }
        return Matriz;
    }

    //Metodos busqueda de amplitud
    public int[] StringAmplitud(int origen) throws PosicionException, TopeException, EstructuraDataVaciaExpetion, VerticeException {
        for (int i = 2; i < StringArray(AmplitudArray(origen)).length; i++) {
            if (origen == StringArray(AmplitudArray(origen))[i]) {
                return null;
            }
        }
        return StringArray(AmplitudArray(origen));
    }

    public int[] AmplitudArray(int origen) throws TopeException, PosicionException, EstructuraDataVaciaExpetion, VerticeException {
        int res[] = new int[numVertices() + 1];
        restaurantesi = new int[numVertices() + 1];
        ordenres = 1;
        cola = new Cola<Integer>("COLA", numVertices());
        for (int i = 1; i <= numVertices(); i++) {
            if (restaurantesi[i] == 0) {
                AmplitudtoArray(origen, res);
            }
        }
        return res;
    }

    protected void AmplitudtoArray(int origen, int res[]) throws TopeException, PosicionException, EstructuraDataVaciaExpetion, VerticeException {
        res[ordenres] = origen;
        restaurantesi[origen] = ordenres++;
        cola.queue(origen);
        while (!cola.estaVacia()) {
            int u = cola.dequeue(origen);
            ListaEnlazada<Adycencia> lista = adycentes(u);
            for (int i = 0; i < lista.getSize(); i++) {
                Adycencia a = lista.obtenerDato(i);
                if (restaurantesi[a.getDestino()] == 0) {
                    res[ordenres] = a.getDestino();
                    restaurantesi[a.getDestino()] = ordenres++;
                    cola.queue(a.getDestino());
                }
            }
        }
    }

    public int[] toArray(int origen) throws VerticeException, PosicionException {
        int res[] = new int[numVertices() + 1];
        restaurantesi = new int[numVertices() + 1];
        ordenres = 1;
        for (int i = 1; i <= numVertices(); i++) {
            if (restaurantesi[i] == 0) {
                Array(origen, res);
            }
        }
        return res;
    }

    protected void Array(int origen, int[] res) throws VerticeException, PosicionException {
        res[ordenres] = origen;
        restaurantesi[origen] = ordenres++;
        ListaEnlazada<Adycencia> lista1 = adycentes(origen);
        for (int i = 0; i < lista1.getSize(); i++) {
            Adycencia a = lista1.obtenerDato(i);
            if (restaurantesi[a.getDestino()] == 0) {
                Array(a.getDestino(), res);
            }
        }
    }

    public int[] String(int origen) throws VerticeException, PosicionException {
        for (int i = 2; i < StringArray(toArray(origen)).length; i++) {
            if (origen == StringArray(toArray(origen))[i]) {
                return null;
            }
        }
        return StringArray(toArray(origen));
    }

    protected int[] StringArray(int[] res) {
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < res.length; i++) {
            sb.append(res[i]).append("\n");
        }
        return res;
    }

    public String MetodoFloyd(float[][] adycencia, int x, int y) {
        int a, b, c;
        float ab, ac, bc, suma, min;
        float[][] matriz = adycencia;
        String caminos[][] = new String[numVertices()][numVertices()];
        String[][] caminosAux = new String[numVertices()][numVertices()];
        String recorrido = "", aux = "";

        for (a = 0; a < numVertices(); a++) {
            for (b = 0; b < numVertices(); b++) {
                caminos[a][b] = "";
                caminosAux[a][b] = "";
            }
        }

        for (c = 0; c < numVertices(); c++) {
            for (a = 0; a < numVertices(); a++) {
                for (b = 0; b < numVertices(); b++) {
                    ab = matriz[a][b];
                    ac = matriz[a][c];
                    bc = matriz[c][b];
                    suma = ac + bc;

                    min = Math.min(ab, suma);
                    if (ab != suma) {
                        if (min == suma) {
                            recorrido = "";
                            caminosAux[a][b] = c + "";
                            caminos[a][b] = caminosR(a, c, caminosAux, recorrido) + (c + 1);

                        }
                    }
                    matriz[a][b] = min;
                }
            }
        }

        for (a = 0; a < x; a++) {
            for (b = 0; b < y; b++) {
                if (matriz[a][b] != 1000000000) {
                    if (a != b) {
                        if (caminos[a][b].equals("")) {
                            aux = "(" + (a + 1) + "," + (b + 1) + ")\n";
                        } else {
                            aux = "(" + (a + 1) + "," + caminos[a][b] + "," + (b + 1) + ")\n";
                        }
                    }
                }
            }
        }
        return aux;
    }

    public String caminosR(int i, int k, String[][] caminosAuxiliares, String caminoRecorrido) {
        if (caminosAuxiliares[i][k].equals("")) {
            return "";
        } else {
            caminoRecorrido += caminosR(i, Integer.parseInt(caminosAuxiliares[i][k].toString()), caminosAuxiliares, caminoRecorrido) + (Integer.parseInt(caminosAuxiliares[i][k].toString()) + 1) + ",";
            return caminoRecorrido;
        }
    }

}
