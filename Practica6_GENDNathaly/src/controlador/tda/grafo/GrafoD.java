/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.tda.grafo;

import controlador.tda.grafo.exception.VerticeException;
import controlador.tda.lista.ListaEnlazada;

/**
 *
 * @author Pc
 */
public class GrafoD extends Grafo{
     
    protected Integer numV;
    protected Integer numA;
    protected ListaEnlazada<Adycencia> listaAdyacente[];
    
    public GrafoD(Integer numV) {
        this.numV = numV;
        this.numA = 0;
        listaAdyacente = new ListaEnlazada[numV + 1];
        for (int i = 1; i <= this.numV; i++) {
            listaAdyacente[i] = new ListaEnlazada<>();
        }
    }
    
    

     @Override
     public Integer numVertices() {
           return this.numV;
     }

     @Override
     public Integer numAristas() {
          return this.numA;
     }

     @Override
     public Object[] existeArista(Integer inicio, Integer fin) throws VerticeException {
          Object[] resultado = {Boolean.FALSE, Double.NaN};
        if (inicio > 0 && fin > 0 && inicio <= numV && fin <= numV) {
            ListaEnlazada<Adycencia> lista = listaAdyacente[inicio];
            for (int i = 0; i < lista.getSize(); i++) {
                try {
                    Adycencia aux = lista.obtenerDato(i);
                    if (aux.getDestino().intValue() == fin.intValue()) {
                        resultado[0] = Boolean.TRUE;
                        resultado[1] = aux.getPeso();
                        break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return resultado;
        } else {
            throw new VerticeException("Algun vertice ingresado no existe");
        }
     }

     @Override
     public Double pesoArista(Integer i, Integer f) throws VerticeException {
           // Double.NaN no hay un dato numerico 
        Double peso = Double.NaN;
        Object[] existe = existeArista(i, f);
        if ((Boolean) (existe[0])) {
            peso = (Double) existe[1];
        }
        return peso;
     }

     @Override
     public void insertarArista(Integer i, Integer j) throws VerticeException {
          insertarArista(i, j, Double.NaN);
     }

     @Override
     public void insertarArista(Integer i, Integer j, Double peso) throws VerticeException {
          if (i > 0 && j > 0 && i <= numV && j <= numV) {
            Object[] existe = existeArista(i, j);
            if (!((Boolean) existe[0])) {
                numA++;
                listaAdyacente[i].insertarCabecera(new Adycencia(j, peso));
            }
        } else {
            throw new VerticeException("Algun vertice ingresado no existe");

        }
     }

     @Override
     public ListaEnlazada<Adycencia> adycentes(Integer i) throws VerticeException {
           return listaAdyacente[i];
     }
     
     public static void main(String[] args) {

        GrafoD grafo = new GrafoD(6);

        try {
            grafo.insertarArista(5, 6);
            grafo.insertarArista(1, 6);
            grafo.insertarArista(4, 5);
            // grafo.insertarAristas(2, 4);
            System.out.println(grafo.existeArista(1, 6)[0]);
            System.out.println(grafo.toString());

        } catch (Exception e) {
            System.out.println("Error" + e);
            e.printStackTrace();
        }

    }
     
    public Boolean existArista(Integer i, Integer j) throws Exception {
        Boolean esta = false;
        if (i.intValue() <= numV && j.intValue() <= numV) {
            ListaEnlazada<Adycencia> lista_adyacentes = listaAdyacente[i];
            for (int k = 0; k < lista_adyacentes.getSize(); k++) {
                Adycencia aux = lista_adyacentes.obtenerDato(k);
                if (aux.getDestino().intValue() == j.intValue()) {
                    esta = true;
                    break;
                }
            }
        }
        return esta;
    }



     

}