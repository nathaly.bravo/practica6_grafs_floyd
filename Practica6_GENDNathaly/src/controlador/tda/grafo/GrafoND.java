/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.tda.grafo;


import controlador.tda.grafo.exception.VerticeException;

/**
 *
 * @author jona
 */
public class GrafoND extends GrafoD {

    public GrafoND(Integer numV) {
        super(numV);
    }

     @Override
     public void insertarArista(Integer i, Integer j, Double peso) throws VerticeException {
          if (i > 0 && j > 0 && i <= numV && j <= numV) {
            Object[] existe = existeArista(i, j);
            if (!((Boolean) existe[0])) {
                numA++;
                listaAdyacente[i].insertarCabecera(new Adycencia(j, peso));
                listaAdyacente[j].insertarCabecera(new Adycencia(i, peso));
            }
        } else {
            throw new VerticeException("Algun vertice ingresado no existe GND");

        }
     }
    


    
    public static void main(String[] args) {
        GrafoND gnd = new GrafoND(6);
        try {
            gnd.insertarArista(1, 4);
            gnd.insertarArista(5, 6);
            System.out.println(gnd.toString());
        } catch (Exception e) {
        }
    }

}
