/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.tda.grafo.exception;

/**
 *
 * @author jona
 */
public class VerticeException extends Exception{

     /**
      * Creates a new instance of <code>VerticesException</code> without detail
      * message.
      */
     public VerticeException() {
     }

     /**
      * Constructs an instance of <code>VerticesException</code> with the
      * specified detail message.
      *
      * @param msg the detail message.
      */
     public VerticeException(String msg) {
          super(msg);
     }
}
