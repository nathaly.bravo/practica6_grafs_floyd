/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.restaurante;

import controller.AdaptadorDao;
import modelo.Restaurante;

/**
 *
 * @author Nathaly
 */
public class RestauranteController extends AdaptadorDao<Restaurante> { 
    
    private Restaurante restau;

    public Restaurante getRestaurante() {
        if(restau == null)
            restau = new Restaurante();
        return restau;
    }

    public void setRestaurante(Restaurante restaurante) {
        this.restau = restaurante;
    }
    
    
    public RestauranteController() {
        super(Restaurante.class);
        listar();
    }
    
    public Boolean guardar(){
        Integer aux = (getGrafo()!= null) ? getGrafo().numVertices() + 1 : 1;
        restau.setId(aux);
        return guardar(restau);
    }
    
    public Boolean modificar(){
        return modificar(restau);
    }
}
