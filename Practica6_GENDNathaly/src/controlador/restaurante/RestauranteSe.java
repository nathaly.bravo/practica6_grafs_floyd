/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.restaurante;

import controlador.tda.grafo.GrafoEND;
import modelo.Restaurante;

/**
 *
 * @author Nathaly
 */
public class RestauranteSe {
    RestauranteController restaurante = new RestauranteController();

    public Restaurante getRestaurante() {
        return restaurante.getRestaurante();
    }

    public void setRestaurante(Restaurante rest) {
        restaurante.setRestaurante(rest);
    }
    
    public boolean guardar(){
        return restaurante.guardar();
    }
    
    public void imprimirGrafo(){
        System.out.println(restaurante.getGrafo().toString());
    }
    
    public boolean actualizarGrafo(GrafoEND<Restaurante> grafoActual){
        return restaurante.actualizar(grafoActual);
    }    
    
     public GrafoEND<Restaurante> getGrafo(){
        return restaurante.listar();
    }
    
    public GrafoEND<Restaurante> getGrafoE(){
        return restaurante.getGrafo();
    }
    
}
