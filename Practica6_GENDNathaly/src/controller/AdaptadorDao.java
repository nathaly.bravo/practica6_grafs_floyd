/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controlador.tda.grafo.Adycencia;
import controlador.tda.grafo.GrafoEND;
import controlador.tda.lista.ListaEnlazada;
import controlador.tda.lista.ListaEnlazadaServices;
import controlador.utiles.Utilidades;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Nathaly
 */
public class AdaptadorDao<E> implements InterfazDao<E> {

    private Class<E> clazz;
    private  String URL = "grafos" + File.separatorChar;
    private GrafoEND<E> grafo;
    
    public AdaptadorDao(Class<E> clazz) {
        this.clazz = clazz;
        URL += this.clazz.getSimpleName() + ".xml";
    }

    public GrafoEND<E> getGrafo() {
        return grafo;
    }

    public void setGrafo(GrafoEND<E> grafo) {
        this.grafo = grafo;
    }
    public void guardarr(E dato) throws Exception {
        ListaEnlazada<E> lista = null;
        listar();
        if(lista.getSize() != 0) {
            lista.insertar(dato, lista.getSize() - 1);
            AgregarVertive();
        } else {
            lista.insertar(dato, 0);
        }
        
        FileOutputStream file = new FileOutputStream(URL);
        JAXBContext jAXBContext = JAXBContext.newInstance(new Class[]{ListaEnlazada.class, this.clazz});
        Marshaller marshaller = jAXBContext.createMarshaller();

        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        marshaller.marshal(lista, file);
    }

   
    public void modificar(E dato, Integer pos) throws Exception {
        ListaEnlazada<E> lista = null;
        listar();
        lista.modificarDato(pos, dato);
        FileOutputStream file = new FileOutputStream(URL);
        JAXBContext jAXBContext = JAXBContext.newInstance(new Class[]{ListaEnlazada.class, this.clazz});
        Marshaller marshaller = jAXBContext.createMarshaller();

        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        marshaller.marshal(lista, file);
    }

    
    public ListaEnlazadaServices<E> listarr() {
        ListaEnlazada<E> lista = new ListaEnlazada<>();
        try {
            //DocumentBuilderFactory dbf = DocumentBuilderFactory.newDefaultInstance();
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();//valido para jdk 1.8
            DocumentBuilder db = dbf.newDocumentBuilder();
           // FileInputStream xmlFile = new FileInputStream(URL);   
            
                    
            //ByteArrayInputStream bis = new ByteArrayInputStream(xmlFile.readAllBytes());
            //Document doc = db.parse(bis);
            Document doc = db.parse(new File(URL));//valido para jdk 1.8
            NodeList datos = doc.getElementsByTagName(this.clazz.getSimpleName().toLowerCase());
            for (int i = 0; i < datos.getLength(); i++) {
                Node n1 = datos.item(i);

                NodeList aux = n1.getChildNodes();
                E objeto = this.clazz.newInstance();
                
                for (int j = 0; j < aux.getLength(); j++) {
                    Node dato = aux.item(j);

                    if (dato.getNodeName() != null && !dato.getNodeName().equals("")
                            && dato.getTextContent() != null && !dato.getTextContent().equals("") && !dato.getNodeName().equals("#text")) {
                        
                        objeto = (E) Utilidades.cambiarDatos(dato.getTextContent(), dato.getNodeName(), objeto);
                        
                    }

                }
                lista.insertar(objeto, lista.getSize() - 1);

            }
            lista.imprimir();
        } catch (Exception e) {
            System.out.println("NO SE PUEDE CARGAR "+e);
            //e.printStackTrace();
        }
        ListaEnlazadaServices<E> listado = new ListaEnlazadaServices<>();
        listado.setLista(lista);
        System.out.println("Hola listado");
        return listado;
    }

    
    
     private void AgregarVertive() throws Exception {
        if (grafo == null) {
            grafo = new GrafoEND<>(1, this.clazz);
        } else {
            GrafoEND aux = new GrafoEND(grafo.numVertices() + 1, this.clazz);
            for (int i = 1; i <= grafo.numVertices(); i++) {
                aux.etiquetarVertice(i, grafo.obtenerEtiqueta(i));
                ListaEnlazada<Adycencia> lista = grafo.adycentes(i);
                for (int j = 0; j < lista.getSize(); j++) {
                    Adycencia ad = lista.obtenerDato(j);
                    aux.insertarArista(i, ad.getDestino(), ad.getPeso());
                }
            }
            grafo = aux;
        }

    }

    @Override
    public boolean guardar(E dato){
         try {
            //guardar en grafo aumentar la dimension 
            listar();
            AgregarVertive();
            grafo.etiquetarVertice(grafo.numVertices(), dato);
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(URL));
            oos.writeObject(grafo);
            oos.close();
            return true;
        } catch (Exception e) {
            System.out.println(" Error en guardar " + e);
        }
        return false;
    }

   @Override
    public boolean modificar(E dato) {

        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(URL));
            grafo = listar();
            oos.writeObject(grafo);
            oos.close();
            return true;
        } catch (Exception e) {
            System.out.println(" Error en guardar " + e);
        }
        return false;
    }

    
    public boolean actualizar(GrafoEND<E> graf){
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(URL));
            oos.writeObject(grafo);
            oos.close();
            return true;
        } catch (Exception e) {
            System.out.println("Error en actualizarGrafo: " + e);
        }
        return false; 
    }
    @Override
    public GrafoEND<E> listar() {
      try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(URL));
            grafo = (GrafoEND<E>) ois.readObject();
            ois.close();
        } catch (Exception e) {
             System.out.println("Error en listar "+e);
        }
        return grafo;
    }


}
