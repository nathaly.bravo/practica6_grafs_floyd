/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controlador.tda.grafo.GrafoEND;
import controlador.tda.lista.ListaEnlazada;
import controlador.tda.lista.ListaEnlazadaServices;

/**
 *
 * @author Nathaly
 */
public interface InterfazDao <T> {
    public boolean guardar(T dato) throws Exception;
    public boolean modificar(T dato) throws Exception;
    public GrafoEND<T> listar();
}
