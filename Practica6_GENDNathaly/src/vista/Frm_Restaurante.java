/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.restaurante.RestauranteSe;
import controlador.tda.grafo.Adycencia;
import controlador.tda.grafo.GrafoEND;
import controlador.tda.grafo.exception.GrafoConexionException;
import controlador.tda.grafo.exception.VerticeException;
import controlador.tda.lista.ListaEnlazada;
import controlador.tda.lista.exception.PosicionException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import vista.modelo.ModeloTablaRestaurante;

/**
 *
 * @author Nathaly
 */
public class Frm_Restaurante extends javax.swing.JDialog {

    /**
     * Creates new form Frm_Restaurante
     */
    private RestauranteSe rest = new RestauranteSe();
    private ModeloTablaRestaurante mtr = new ModeloTablaRestaurante();

    public Frm_Restaurante(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        try {
            cargarTabla();
        } catch (Exception ex) {
            Logger.getLogger(Frm_Restaurante.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //Metodo de limpiar
    private void limpiar() throws Exception {
        txtUbi.setText("");
        txtNombre.setText("");
        txtPeso.setText("");
        rest.setRestaurante(null);
        cargarTabla();
        cargarVista();
    }
//Metodo de Cargar Tabla

    private void cargarTabla() throws Exception {
        if (rest.getGrafo() != null) {
            mtr.setGend(rest.getGrafo());
            tbl_tabla.setModel(mtr);
            mtr.fireTableStructureChanged();
            tbl_tabla.updateUI();
        } else {
            DefaultTableModel model_tabla = new DefaultTableModel(2, 2);
            tbl_tabla.setModel(model_tabla);
            tbl_tabla.updateUI();
        }
    }
//Metodo de Cargar vista en combos

    private void cargarVista() throws Exception {
        cbxOrigen.removeAllItems();
        cbxDestino.removeAllItems();
        if (rest.getGrafo() != null) {
            for (int i = 0; i < rest.getGrafo().numVertices(); i++) {
                String etiqueta = rest.getGrafo().obtenerEtiqueta((i + 1)).toString();
                cbxOrigen.addItem(etiqueta);
                cbxDestino.addItem(etiqueta);
            }
        }
    }
    //Metodo de Validar Matriz

    public float[][] MatrizValidar(float matrix[][]) {
        try {
            for (int i = 0; i < rest.getGrafo().numVertices(); i++) {
                for (int j = 0; j < rest.getGrafo().numVertices(); j++) {
                    if (rest.getGrafo().GenerarM()[i][j] == 0) {
                        matrix[i][j] = 100000000;
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("ERROR");
        }
        return matrix;
    }
//Metodo de guardar

    private void guardar() {
        if (txtNombre.getText().trim().length() > 0 && txtUbi.getText().trim().length() > 0) {
            try {
                rest.getRestaurante().setNombre(txtNombre.getText());
                rest.getRestaurante().setUbicacion(txtUbi.getText());
                if (rest.guardar()) {
                    JOptionPane.showMessageDialog(null, "GUARDADO CON EXITO", "OK", JOptionPane.INFORMATION_MESSAGE);
                    limpiar();
                } else {
                    JOptionPane.showMessageDialog(null, "NO SE GUARDO", "ERROE", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "CAMPOS VACIOS", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }
//Metodo de agregar peso o  adyacencia

    private void Adyacencia() throws VerticeException, Exception {
        if (rest.getGrafo() != null) {
            if (cbxDestino.getSelectedIndex() == cbxOrigen.getSelectedIndex()) {
                JOptionPane.showMessageDialog(null, "NO SE PUEDE INSERTAR ARISTA", "ERROR", JOptionPane.ERROR_MESSAGE);
            } else {
                if (!txtPeso.getText().isEmpty()) {
                    Integer origen = cbxOrigen.getSelectedIndex();
                    Integer destino = cbxDestino.getSelectedIndex();
                    Double dis = (Double.parseDouble(txtPeso.getText()));
                    Double tiempo = (dis/100) * 60;
                    rest.getGrafo().insertarArista((origen + 1), (destino + 1), tiempo);
                    if (rest.actualizarGrafo(rest.getGrafoE())) {
                        JOptionPane.showMessageDialog(null, "GRAFO ACTUALIZADO EXITOSAMENTE ", "OK", JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(null, "CAMPOS VACIOS", "ERROR", JOptionPane.ERROR_MESSAGE);
                    }
                    limpiar();
                } else {
                    JOptionPane.showMessageDialog(null, "COMPLETE CAMPOS VACIOS ", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
    //Metodo de profundidad
    private void Profundidad() throws VerticeException, PosicionException, Exception {
        Integer inicio = Integer.parseInt(JOptionPane.showInputDialog("INGRESE NODO DE INICIO"));
        if (inicio > rest.getGrafoE().numVertices() || inicio <= 0) {
            JOptionPane.showMessageDialog(null, "NODO NO EXISTENTE");
        } else {
            String a = null;
            for (int i = 4; i < Arrays.toString(rest.getGrafoE().String(inicio)).length(); i += 3) {
                String h = Arrays.toString(rest.getGrafoE().String(inicio));
                a = a + "--" + rest.getGrafoE().obtenerEtiqueta(Character.getNumericValue(h.charAt(i))).getNombre();
            }
            if (rest.getGrafoE().String(inicio) == null) {
                JOptionPane.showMessageDialog(null, "NO EXISTEN SUFICIENTES ADYACENCIAS");
            } else {
                txtBusqueda.append("BUSQUEDA DE PROFUNDIDAD: " + a.substring(6));
            }
        }
    }
    

    //Metodo de anchura
    private void Anchura() throws Exception {
        Integer inicio = Integer.parseInt(JOptionPane.showInputDialog("INGRESE NODO DE INICIO"));
        if (inicio > rest.getGrafoE().numVertices() || inicio <= 0) {
            JOptionPane.showMessageDialog(null, "NODO NO EXISTENTE");
        } else {
            String x = null;
            for (int i = 4; i < Arrays.toString(rest.getGrafoE().StringAmplitud(inicio)).length(); i += 3) {
                String h = Arrays.toString(rest.getGrafoE().StringAmplitud(inicio));
                x = x + "--" + rest.getGrafoE().obtenerEtiqueta(Character.getNumericValue(h.charAt(i))).getNombre();
            }
            if (rest.getGrafoE().StringAmplitud(inicio) == null) {
                JOptionPane.showMessageDialog(null, "NO EXISTEN SUFICIENTES ADYACENCIAS ");
            } else {
                txtBusqueda.append("BUSQUEDA DE ANCHURA " + x.substring(6));
            }
        }
    }

//Metodo Floyd

    private void Floyd() {
        if (cbxDestino.getSelectedIndex() == cbxOrigen.getSelectedIndex()) {
            JOptionPane.showMessageDialog(null, "NO SE PUEDE INSERTAR ARISTA CON EL MISMO VALOR", "ERROR", JOptionPane.ERROR_MESSAGE);
        } else {
            Integer or = cbxOrigen.getSelectedIndex() + 1;
            Integer dest = cbxDestino.getSelectedIndex() + 1;
            try {
                String aux = null;
                float auxMatriz[][] = rest.getGrafo().GenerarM();
                auxMatriz = MatrizValidar(auxMatriz);
                String h = rest.getGrafo().MetodoFloyd(auxMatriz, or, dest);
                for (int i = 1; i < h.length() - 1; i += 2) {
                    aux = aux + ">>" + rest.getGrafoE().obtenerEtiqueta(Character.getNumericValue(h.charAt(i))).getUbicacion();
                }
                JOptionPane.showMessageDialog(null, "El camino mas corto es: " + rest.getGrafoE().obtenerEtiqueta(or).getNombre() + " a el restaurante: " + rest.getGrafoE().obtenerEtiqueta(dest).getNombre() + "\n es desde "
                        + aux.substring(6));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        txtNombre = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtUbi = new javax.swing.JTextField();
        BtnGuardar = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        cbxOrigen = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        cbxDestino = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        txtPeso = new javax.swing.JTextField();
        BtnVincular = new javax.swing.JButton();
        BtnFloyd = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_tabla = new javax.swing.JTable();
        BtnMostrar = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtBusqueda = new javax.swing.JTextArea();
        BtnAnchura = new javax.swing.JButton();
        BtnProfundidad = new javax.swing.JButton();

        jLabel4.setText("jLabel4");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel1.setBackground(new java.awt.Color(255, 255, 153));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos Restaurante", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 1, 14))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel2.setText("Ubicación");

        BtnGuardar.setBackground(new java.awt.Color(102, 255, 153));
        BtnGuardar.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        BtnGuardar.setText("Guardar");
        BtnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnGuardarActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel7.setText("Nombre");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtUbi, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(41, 41, 41)
                        .addComponent(BtnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(BtnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtUbi, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 153));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 1, 14))); // NOI18N

        jLabel9.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel9.setText("Origen");

        cbxOrigen.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel6.setText("Destino");

        cbxDestino.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel8.setText("Peso");

        BtnVincular.setBackground(new java.awt.Color(102, 255, 153));
        BtnVincular.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        BtnVincular.setText("Vincular");
        BtnVincular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnVincularActionPerformed(evt);
            }
        });

        BtnFloyd.setBackground(new java.awt.Color(102, 255, 153));
        BtnFloyd.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        BtnFloyd.setText("Floyd");
        BtnFloyd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnFloydActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbxOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbxDestino, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPeso, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(BtnFloyd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BtnVincular, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                .addGap(32, 32, 32))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbxOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BtnVincular, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(8, 8, 8)
                        .addComponent(cbxDestino, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtPeso, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(BtnFloyd, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 153));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tbl_tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tbl_tabla);

        BtnMostrar.setBackground(new java.awt.Color(102, 255, 153));
        BtnMostrar.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        BtnMostrar.setText("Mostrar");
        BtnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnMostrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(152, Short.MAX_VALUE)
                .addComponent(BtnMostrar, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(130, 130, 130))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(BtnMostrar, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(13, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 153));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 1, 14))); // NOI18N

        txtBusqueda.setColumns(20);
        txtBusqueda.setRows(5);
        jScrollPane2.setViewportView(txtBusqueda);

        BtnAnchura.setBackground(new java.awt.Color(102, 255, 153));
        BtnAnchura.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        BtnAnchura.setText("Anchura");
        BtnAnchura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAnchuraActionPerformed(evt);
            }
        });

        BtnProfundidad.setBackground(new java.awt.Color(102, 255, 153));
        BtnProfundidad.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        BtnProfundidad.setText("Profundidad");
        BtnProfundidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnProfundidadActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BtnProfundidad, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnAnchura, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(BtnProfundidad, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(BtnAnchura, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(122, 122, 122))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 493, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnGuardarActionPerformed
        guardar();
    }//GEN-LAST:event_BtnGuardarActionPerformed

    private void BtnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnMostrarActionPerformed
        new Grafo(null, true, rest).setVisible(true);
    }//GEN-LAST:event_BtnMostrarActionPerformed

    private void BtnAnchuraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAnchuraActionPerformed
        try {
            Anchura();
        } catch (Exception ex) {
            Logger.getLogger(Frm_Restaurante.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_BtnAnchuraActionPerformed

    private void BtnProfundidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnProfundidadActionPerformed
        try {
            Profundidad();
        } catch (PosicionException ex) {
            Logger.getLogger(Frm_Restaurante.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Frm_Restaurante.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_BtnProfundidadActionPerformed

    private void BtnVincularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnVincularActionPerformed
        try {
            // TODO add your handling code here:
            Adyacencia();
        } catch (VerticeException ex) {
            Logger.getLogger(Frm_Restaurante.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Frm_Restaurante.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_BtnVincularActionPerformed

    private void BtnFloydActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnFloydActionPerformed
        // TODO add your handling code here:
        Floyd();
    }//GEN-LAST:event_BtnFloydActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frm_Restaurante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frm_Restaurante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frm_Restaurante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frm_Restaurante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Frm_Restaurante dialog = new Frm_Restaurante(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnAnchura;
    private javax.swing.JButton BtnFloyd;
    private javax.swing.JButton BtnGuardar;
    private javax.swing.JButton BtnMostrar;
    private javax.swing.JButton BtnProfundidad;
    private javax.swing.JButton BtnVincular;
    private javax.swing.JComboBox<String> cbxDestino;
    private javax.swing.JComboBox<String> cbxOrigen;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tbl_tabla;
    private javax.swing.JTextArea txtBusqueda;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtPeso;
    private javax.swing.JTextField txtUbi;
    // End of variables declaration//GEN-END:variables

}
