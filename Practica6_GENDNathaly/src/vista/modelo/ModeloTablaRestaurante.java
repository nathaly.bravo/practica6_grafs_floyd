/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.modelo;

import controlador.tda.grafo.GrafoEND;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Nathaly
 */
public class ModeloTablaRestaurante extends AbstractTableModel{

    private GrafoEND Grafoend;
    private String[] fila_columna;

    public GrafoEND getGend() {
        return Grafoend;
    }

    public void setGend(GrafoEND Grafoend) throws Exception {
        this.Grafoend = Grafoend;
        Columnas();
    }

    public String[] getColumnas() {
        return fila_columna;
    }

    public void setColumnas(String[] columnas) {
        this.fila_columna = columnas;
    }

    @Override
    public int getRowCount() {
        return Grafoend.numVertices();
    }

    @Override
    public int getColumnCount() {
        return Grafoend.numVertices() + 1;
    }

    private String[] Columnas() throws Exception {
        fila_columna = new String[Grafoend.numVertices() + 1];
        fila_columna[0] = "-";
        for (int i = 1; i < fila_columna.length; i++) {
            fila_columna[i] = Grafoend.obtenerEtiqueta(i).toString();
        }
        return fila_columna;
    }

    @Override
    public Object getValueAt(int i0, int i1) {
        if (i1 == 0) {
            return fila_columna[i0 + 1];
        } else {
            try {
                if (Grafoend.existArista((i0 + 1), i1)) {
                    return Grafoend.pesoArista((i0 + 1), i1);
                } else {
                    return "--";
                }
            } catch (Exception e) {
                System.out.println("ERROR");
            }
        }
        return "";
    }

    @Override
    public String getColumnName(int clm) {
        return fila_columna[clm];
    }

}
